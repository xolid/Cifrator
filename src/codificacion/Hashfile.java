package codificacion;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Esta clase lee un archivo y convierte su contenido en MD5 o SHA
 * Imprime por pantalla el resultado.
 */
public class Hashfile {
 
    public String md(String f, String hash){
        String res="";
        try{
            FileInputStream fis = new FileInputStream(f);
            MessageDigest md = MessageDigest.getInstance(hash);
            try (DigestInputStream dis = new DigestInputStream(fis, md)) {
                byte[] b = new byte[32*1024];
                int i = 0;
                do{
                    i = dis.read(b, 0, 32*1024);
                }while(i == 32*1024);
                res = byteArrayToHexString(md.digest());
            }
        }catch(NoSuchAlgorithmException | IOException e){
            JOptionPane.showMessageDialog(new JFrame(),"Error al cifrar la semilla...");
        }
        return res;
    }
 
    /**
     * byteArrayToHexString
     * Convierte un array de bytes a un String hexadecimal
     * @param b
     * @return 
     */
    public String byteArrayToHexString(byte[] b){
        StringBuilder sb = new StringBuilder(b.length * 2);
        for(int i = 0; i < b.length; i++){
            int v = b[i] & 0xff;
            if(v < 16){
                sb.append("0");
            }
            sb.append(Integer.toHexString(v));
        }
        return sb.toString();
    }
}